from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models


class Shop(AbstractUser):
    shopify_domain = models.CharField(max_length=100, blank=True, null=True, unique=True)
    shopify_token = models.CharField(max_length=200, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    phone_number = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    zip = models.CharField(max_length=100, blank=True, null=True)
    status = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'shopify_domain'
    REQUIRED_FIELDS = []
