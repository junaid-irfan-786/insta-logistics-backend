from Logistics.CourierBusinessLogic import CourierBusinessLogic
from Logistics.Decorators import start_new_thread
from Logistics.ShopifyBusinessLogic import ShopifyBusinessLogic
from Logistics.models import FulfilledOrders, OrderLineItems


class BusinessLogic(ShopifyBusinessLogic, CourierBusinessLogic):

    def __init__(self, shop):
        CourierBusinessLogic.__init__(self, shop)
        ShopifyBusinessLogic.__init__(self, shop)

    @start_new_thread
    def fulfillOrders(self, orders):
        orders = self.fetchAndCreateOrdersInDb(orders)
        for order in orders:
            cn_generation = False
            if not order.cn_generated:
                cn_number = self.generateCN(order)
                if cn_number is not None:
                    cn_generation = True
                    order.cn_number = cn_number
                    order.cn_generated = cn_generation
                    order.save(update_fields=['cn_number', 'cn_generated'])
            else:
                if order.is_edited:
                    is_cancelled = self.cancelCN(order)
                    if is_cancelled:
                        cn_number = self.generateCN(order)
                        if cn_number is not None:
                            cn_generation = True
                            order.cn_number = cn_number
                            order.cn_generated = cn_generation
                            order.save(update_fields=['cn_number', 'cn_generated'])
                else:
                    cn_generation = True

            if not order.fulfilled_on_shopify:
                if cn_generation:
                    fulfilment_id = self.fulfillOrderOnShopify(order)
                    if fulfilment_id is not None:
                        order.fulfilled_on_shopify = True
                        order.fulfillment_id = fulfilment_id
                        order.fulfillment_status = "fulfilled"
                        order.save(update_fields=['fulfilled_on_shopify', 'fulfillment_id', 'fulfillment_status'])

    @start_new_thread
    def cancelOrders(self, orders):
        for order in orders:
            if order.cn_generated:
                cn_cancelled = self.cancelCN(order)
                if cn_cancelled:
                    order.cn_number = None
                    order.cn_generated = False
                    order.save(update_fields=['cn_number', 'cn_generated'])

            if order.fulfilled_on_shopify:
                if not order.cn_generated:
                    status = self.cancelOrderOnShopify(order)
                    if status:
                        order.fulfillment_id = None
                        order.fulfilled_on_shopify = False
                        order.fulfillment_status = "unfulfilled"
                        order.save(update_fields=['fulfilled_on_shopify', 'fulfillment_id', 'fulfillment_status'])

    def fetchAndCreateOrdersInDb(self, orders):
        shopify_order_ids = {}
        for order in orders:
            shopify_order_ids[order['order_id']] = order

        fulfilled_orders = FulfilledOrders.objects.filter(order_id__in=shopify_order_ids.keys()). \
            prefetch_related('fulfilled_lines')

        for shopify_order_id in shopify_order_ids:
            check = False
            for fulfilled_order in fulfilled_orders:
                if shopify_order_id == fulfilled_order.order_id:
                    check = True
                    break

            if not check:
                line_items = shopify_order_ids[shopify_order_id].pop('line_items')
                order = FulfilledOrders.objects.create(**shopify_order_ids[shopify_order_id],
                                                       shop=self.shop)
                for line_item in line_items:
                    variant_detail = self.getVariantDetail(line_item['product_id'],
                                                           line_item['variant_id'])
                    if variant_detail is not None:
                        line_item['barcode'] = variant_detail['barcode']
                        line_item['option1'] = variant_detail['option1']
                        line_item['option2'] = variant_detail['option2']
                        line_item['option3'] = variant_detail['option3']
                    OrderLineItems.objects.create(order=order, **line_item)

        fulfilled_orders = FulfilledOrders.objects.filter(order_id__in=shopify_order_ids.keys()). \
            prefetch_related('fulfilled_lines')

        for fulfilled_order in fulfilled_orders:
            is_edited = False
            if fulfilled_order.total_price != shopify_order_ids[fulfilled_order.order_id]['total_price']:
                fulfilled_order.total_price = shopify_order_ids[fulfilled_order.order_id]['total_price']
                is_edited = True
            if fulfilled_order.customer_email != shopify_order_ids[fulfilled_order.order_id]['customer_email']:
                fulfilled_order.customer_email = shopify_order_ids[fulfilled_order.order_id]['customer_email']
                is_edited = True
            if fulfilled_order.customer_name != shopify_order_ids[fulfilled_order.order_id]['customer_name']:
                fulfilled_order.customer_name = shopify_order_ids[fulfilled_order.order_id]['customer_name']
                is_edited = True
            if fulfilled_order.customer_phone != shopify_order_ids[fulfilled_order.order_id]['customer_phone']:
                fulfilled_order.customer_phone = shopify_order_ids[fulfilled_order.order_id]['customer_phone']
                is_edited = True
            if fulfilled_order.customer_city != shopify_order_ids[fulfilled_order.order_id]['customer_city']:
                fulfilled_order.customer_city = shopify_order_ids[fulfilled_order.order_id]['customer_city']
                is_edited = True
            if fulfilled_order.customer_address1 != shopify_order_ids[fulfilled_order.order_id]['customer_address1']:
                fulfilled_order.customer_address1 = shopify_order_ids[fulfilled_order.order_id]['customer_address1']
                is_edited = True
            if fulfilled_order.customer_address2 != shopify_order_ids[fulfilled_order.order_id]['customer_address2']:
                fulfilled_order.customer_address2 = shopify_order_ids[fulfilled_order.order_id]['customer_address2']
                is_edited = True
            if fulfilled_order.note != shopify_order_ids[fulfilled_order.order_id]['note']:
                fulfilled_order.note = shopify_order_ids[fulfilled_order.order_id]['note']
                is_edited = True
            if fulfilled_order.service_type != shopify_order_ids[fulfilled_order.order_id]['service_type']:
                fulfilled_order.service_type = shopify_order_ids[fulfilled_order.order_id]['service_type']
                is_edited = True

            if is_edited:
                fulfilled_order.is_edited = is_edited
                fulfilled_order.save(update_fields=['total_price', 'customer_email', 'customer_name',
                                                    'customer_phone', 'customer_city', 'customer_address1',
                                                    'customer_address2', 'note', 'is_edited', 'service_type'])

        return fulfilled_orders