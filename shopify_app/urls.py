from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
import os
from shopify_app.scheduler import every_12_am


urlpatterns = [
    path('admin/', admin.site.urls),
    url('auth/', include('authentication.urls')),
    url('logistics/', include('Logistics.urls')),
]

try:
    if os.environ["production"]:
        every_12_am()
except Exception as e:
    print("Exception in Main url: " + str(e))