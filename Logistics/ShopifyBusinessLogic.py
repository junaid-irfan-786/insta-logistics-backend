import json
import time
import requests

from Logistics.models import Settings, Logs
from shopify_app import settings


class ShopifyBusinessLogic(object):
    def __init__(self, shop):
        self.shop = shop

        try:
            self.setting = shop.shop_setting
        except Settings.DoesNotExist:
            self.setting = None

    def getSingleObjectFromShopify(self, limit, next_cursor, filter):
        url = "https://{}/admin/api/{}/orders.json".format(self.shop.shopify_domain, settings.SHOPIFY_API_VERSION)

        objects = []
        previous_cursor = ""

        if next_cursor == "":
            if filter != "":
                url = url + "?{}&limit={}".format(filter, limit)
            else:
                url = url + "?limit={}".format(limit)
        else:
            url = url + "?limit={}&page_info={}".format(limit, next_cursor)

        headers = {
            "X-Shopify-Access-Token": self.shop.shopify_token,
            "Content-Type": "application/json"
        }

        object = requests.get(url, headers=headers)
        if object.status_code == 200:
            page_info = object.links
            data = object.json()
            objects = objects + data['orders']
            if "next" in page_info:
                next_cursor = page_info["next"]["url"].split('page_info=')[1]
            else:
                next_cursor = ""
            if "previous" in page_info:
                previous_cursor = page_info["previous"]["url"].split('page_info=')[1]

        elif object.status_code == 429:
            pass
        else:
            pass

        obj = {'orders': objects, 'next': next_cursor, 'previous': previous_cursor}
        return obj

    def getVariantDetail(self, product_id, variant_id):
        variant = None

        i = 0
        while i < 3:
            url = "https://{}/admin/api/{}/products/{}/variants/{}.json".format(self.shop.shopify_domain,
                                                                                settings.SHOPIFY_API_VERSION,
                                                                                product_id, variant_id)
            headers = {
                "X-Shopify-Access-Token": self.shop.shopify_token
            }

            resp = requests.get(url, headers=headers)
            if resp.status_code == 200:
                loc_json = resp.json()
                variant = loc_json['variant']
                break
            else:
                i = i + 1
                time.sleep(2)

        return variant

    def getLocationsFromShopify(self):

        i = 0
        while i < 3:
            url = "https://{}/admin/api/{}/locations.json".format(self.shop.shopify_domain, settings.SHOPIFY_API_VERSION)
            headers = {
                "X-Shopify-Access-Token": self.shop.shopify_token
            }

            locations = []
            resp = requests.get(url, headers=headers)
            if resp.status_code == 200:
                loc_json = resp.json()
                for loc in loc_json["locations"]:
                    locations.append({
                        "id": loc["id"],
                        "name": loc["name"]
                    })
                return locations
            else:
                i = i + 1
                time.sleep(2)

        return []

    def fulfillOrderOnShopify(self, order):
        fufillment_id = None

        i = 0
        shopify_id = order.order_id
        cn = order.cn_number
        while i < 3:
            fulfillment_link = "https://" + self.shop.shopify_domain + "/admin/api/" + settings.SHOPIFY_API_VERSION \
                               + "/orders/" + str(shopify_id) + "/fulfillments.json"
            headers = {
                "X-Shopify-Access-Token": self.shop.shopify_token,
                "Content-Type": "application/json"
            }
            fulfillment_data = {
                "fulfillment": {
                    "location_id": self.setting.shopify_location_id,
                    "tracking_number": str(cn),
                    "tracking_company": "InstaLogistics",
                    "tracking_url": "url"
                }
            }
            fulfillment_data = json.dumps(fulfillment_data)
            fulfillment_resp = requests.post(fulfillment_link, data=fulfillment_data, headers=headers)
            data = json.loads(fulfillment_resp.text)

            if fulfillment_resp.status_code == 201:
                fufillment_id = data["fulfillment"]["id"]
                break
            else:
                i = i + 1
                time.sleep(2)

        log_status = "Success"
        msg = "Fulfilled On Shopify"
        if fufillment_id is None:
            log_status = "Failed"
            msg = "Failed to fulfill on Shopify"

        log = Logs.objects.create(order=order)
        log.status = log_status
        log.type = "Shopify Fulfillment"
        log.detail = msg
        log.save(update_fields=['status', 'detail', 'type'])

        return fufillment_id

    def cancelOrderOnShopify(self, order):
        status = False
        fulfillment_id = order.fulfillment_id
        i = 0
        while i < 3:
            fulfillment_link = "https://" + self.shop.shopify_domain + "/admin/api/2020-07/fulfillments/" \
                               + fulfillment_id + "/cancel.json"
            headers = {
                "X-Shopify-Access-Token": self.shop.shopify_token,
                "Content-Type": "application/json"
            }
            resp = requests.post(fulfillment_link, headers=headers)
            if resp.status_code == 200:
                status = True
                break
            else:
                i = i + 1
                time.sleep(2)

        log_status = "Success"
        msg = "Shopify Fulfillment Cancelled"
        if not status:
            log_status = "Failed"
            msg = "Failed to cancel shopify fulfillment"

        log = Logs.objects.create(order=order)
        log.type = "Shopify Cancellation"
        log.status = log_status
        log.detail = msg
        log.save(update_fields=['status', 'detail', 'type'])

        return status

    def updateOrderAddressOnShopify(self, obj):
        status = False
        i = 0
        while i < 3:
            headers = {
                "X-Shopify-Access-Token": self.shop.shopify_token,
                "Content-Type": "application/json"
            }

            updated_order = {
                "order": {
                    "id": obj['order_id'],
                    "shipping_address": {
                        "address1": obj['address1'],
                        "address2": obj['address2'],
                        "city": obj['city'],
                        "phone": obj['phone']
                    }
                }
            }
            updated_order = json.dumps(updated_order)
            order_update_link = "https://" + self.shop.shopify_domain + "/admin/api/" + settings.SHOPIFY_API_VERSION \
                                + "/orders/" + obj['order_id'] + ".json"
            order_resp = requests.put(order_update_link, headers=headers, data=updated_order)
            if order_resp.status_code == 200:
                status = True
                break
            else:
                i = i + 1
                time.sleep(2)

        return status
