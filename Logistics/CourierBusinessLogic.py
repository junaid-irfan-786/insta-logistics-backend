import requests

from Logistics.models import Logs


class CourierBusinessLogic(object):

    def __init__(self, shop):
        self.shop = shop
        self.courier_credentials = self.shop.shop_courier

    def generateCN(self, order):
        cn_number = None
        msg = ""

        lineItems = order.fulfilled_lines.all()
        weight_in_grams = 0
        product_details = ""
        for lineItem in lineItems:
            weight_in_grams += lineItem.product_weight if lineItem.product_weight else 0
            product_details += lineItem.product_name + " " + lineItem.sku_code + " " + "Qty=" \
                               + str(lineItem.product_quantity) + ", "

        address = ""
        address = address + order.customer_address1
        if address != "":
            address += " " + order.customer_address2
        else:
            address += order.customer_address2

        if order.service_type is not None:
            service_type = order.service_type
        else:
            service_type = self.courier_credentials.service_type

        data = {
            "type": self.courier_credentials.type,
            "service_type": service_type,
            "retur": 0,
            "retur_type": 'colet',
            "ramburs": order.total_price,
            "ramburs_type": "cash",
            "payer": 'client',
            "weight": weight_in_grams,
            "content": product_details,
            "comments": order.note if order.note is not None else "",
            "cnt": 1,
            "customer_reference": order.order_name,
            "fragile": self.courier_credentials.fragile,
            "use_default_from_address": True,
            "from_name": self.shop.name,
            "from_contact": self.shop.phone_number,
            "from_address": self.shop.address,
            "from_city": self.shop.city,
            "from_email": self.shop.email,
            "from_phone": self.shop.phone_number,
            "from_county": "",
            "to_name": order.customer_name,
            "to_contact": order.customer_phone,
            "to_address": address,
            "to_city": order.customer_city,
            "to_county": "",
            "to_country": "Pakistan",
            "to_email": order.customer_email,
            "to_phone": order.customer_phone,
            "to_extra": product_details,
        }

        try:
            request = requests.post(
                f"https://app.couriermanager.eu/cscourier/API/create_awb?api_key={self.courier_credentials.api_key}",
                data=data,
                headers={'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'}
            )

            result = request.json()
            if request.status_code == 200:
                if len(result['data']['errors']) == 0:
                    temp = result["data"]['no']
                    cn_number = temp
                    msg = "CN Generated Successfully"
                else:
                    msg = result['data']['errors']
            else:
                msg = "Status code from the courier api should be 200"

        except Exception as e:
            msg = "Courier server is not responding"

        status = "Success"
        if cn_number is None:
            status = "Failed"

        log = Logs.objects.create(order=order)
        log.status = status
        log.type = "CN Generation"
        log.detail = msg
        log.save(update_fields=['status', 'detail', 'type'])

        return cn_number

    def cancelCN(self, order):
        status = False
        msg = ""

        try:
            request = requests.get(
                f"https://app.couriermanager.eu/cscourier/API/cancel?api_key={self.courier_credentials.api_key}"
                f"&awbno={order.cn_number}"
            )

            if request.status_code == 200:
                result = request.json()
                if result["message"] == "Awb canceled":
                    status = True
                    msg = "CN Cancelled"
                else:
                    msg = result["message"]
            else:
                msg = "Status code from the courier api should be 200"

        except Exception as e:
            msg = "Courier server is not responding"

        log_status = "Success"
        if not status:
            log_status = "Failed"

        log = Logs.objects.create(order=order)
        log.type = "CN Cancellation"
        log.status = log_status
        log.detail = msg
        log.save(update_fields=['status', 'detail', 'type'])

        return status

    @staticmethod
    def fetchCities(api_key):
        cities = []
        params = {'api_key': api_key}
        url = 'https://app.couriermanager.eu/cscourier/API/list_cities'
        response = requests.get(url, params)
        if response.status_code == 200:
            response = response.json()

            try:
                status = response['status']
            except:
                cities = response

        return cities