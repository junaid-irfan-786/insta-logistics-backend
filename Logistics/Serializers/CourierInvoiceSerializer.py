from rest_framework import serializers
from Logistics.models import FulfilledOrders, OrderLineItems
from shopify_app import settings


class InvoiceLineItemSerializer(serializers.ModelSerializer):
    options = serializers.SerializerMethodField('getOptions')

    class Meta:
        model = OrderLineItems
        fields = '__all__'

    def getOptions(self, instance):
        arr = []
        if instance.option1 != "Option Unavailable":
            arr.append(instance.option1)
        if instance.option2 != "Option Unavailable":
            arr.append(instance.option2)
        if instance.option3 != "Option Unavailable":
            arr.append(instance.option3)
        return arr


class CourierInvoiceSerializer(serializers.ModelSerializer):
    lineItems = InvoiceLineItemSerializer(many=True, source="fulfilled_lines")
    paidAmount = serializers.SerializerMethodField("getPaidAmount")
    payment_method = serializers.SerializerMethodField("getPaymentMethod")
    date = serializers.SerializerMethodField('getShopifyOrderDate')
    courier_img = serializers.SerializerMethodField('getCourierImage')
    line_img = serializers.SerializerMethodField('getLineImage')
    display_amount = serializers.SerializerMethodField('getDisplayAmount')

    class Meta:
        model = FulfilledOrders
        fields = '__all__'

    def getDisplayAmount(self, data):
        if data.financial_status == 'paid':
            return 0
        else:
            return data.total_price

    def getPaidAmount(self, instance):
        if instance.financial_status == 'paid':
            return instance.subtotal_price
        else:
            return 0

    def getPaymentMethod(self, data):
        method = data.financial_status
        if method == "paid":
            method = "Paid"
        else:
            method = "Cash on Delivery (COD)"

        return method

    def getShopifyOrderDate(self, data):
        date = data.created_at

        date_format = '%d/%m/%Y'
        order_date = date.strftime(date_format)

        return order_date

    def getCourierImage(self, data):
        return settings.HOST + "/static/instaLogistics.jpg"

    def getLineImage(self, data):
        return settings.HOST + '/static/line.jpg'

