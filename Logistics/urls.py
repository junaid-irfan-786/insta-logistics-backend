from django.urls import path
from Logistics.Views.CancelOrder import CancelOrderView
from Logistics.Views.Cities import AllCitiesView
from Logistics.Views.EditOrder import EditOrderAddress
from Logistics.Views.FulfillOrder import FulfillOrderView
from Logistics.Views.FulfilledOrdersView import FulfilledOrdersView, OrderTrackingView
from Logistics.Views.Invoice import SingleInvoice
from Logistics.Views.LogsView import LogsView
from Logistics.Views.Orders import UnfulfilledOrders
from Logistics.Views.ServiceType import ServiceTypeView
from Logistics.Views.Settings import SettingView
from Logistics.Views.ShopifyLocations import ShopifyLocationsView
from Logistics.Views.Stepper import StepOneView, StepTwoView, StepThreeView

urlpatterns = [
    path('fulfillOrder', FulfillOrderView.as_view()),
    path('getFulfilledOrder', FulfilledOrdersView.as_view()),
    path('cancelOrder', CancelOrderView.as_view()),
    path('unfilledOrders', UnfulfilledOrders.as_view()),
    path('edit_order_address', EditOrderAddress.as_view()),
    path('settings', SettingView.as_view()),
    path('cities', AllCitiesView.as_view()),
    path('stepperOne', StepOneView.as_view()),
    path('stepperTwo', StepTwoView.as_view()),
    path('stepperThree', StepThreeView.as_view()),
    path('shopifyLocations', ShopifyLocationsView.as_view()),
    path('invoice', SingleInvoice.as_view()),
    path('logs', LogsView.as_view()),
    path('track_order', OrderTrackingView.as_view()),
    path('service_type', ServiceTypeView.as_view()),
]
