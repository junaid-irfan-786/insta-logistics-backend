from rest_framework import serializers
from Logistics.models import FulfilledOrders


class FulfilledSerializer(serializers.ModelSerializer):

    class Meta:
        model = FulfilledOrders
        fields = '__all__'


class FulfilledOrderIdRequiredSerializer(serializers.Serializer):
    id = serializers.IntegerField()
