from rest_framework import serializers
from Logistics.models import ServiceTypes


class ServiceTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceTypes
        fields = "__all__"