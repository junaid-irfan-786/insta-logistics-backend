from rest_framework import serializers
from Logistics.models import Logs, FulfilledOrders


class OrderLogsSerializer(serializers.ModelSerializer):
    class Meta:
        model = FulfilledOrders
        fields = ('order_name',)


class LogsSerializer(serializers.ModelSerializer):
    order = OrderLogsSerializer()
    created_at = serializers.DateTimeField(format='%d/%m/%Y %I:%M %p')

    class Meta:
        model = Logs
        fields = ('created_at', 'status', 'type', 'detail', 'order')