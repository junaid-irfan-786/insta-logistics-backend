from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.Serializers.EditAddressSerializer import EditOrderSerializer
from Logistics.ShopifyBusinessLogic import ShopifyBusinessLogic


class EditOrderAddress(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        ser = EditOrderSerializer(data=request.data)
        ser.is_valid(raise_exception=True)
        bl = ShopifyBusinessLogic(request.user)
        status = bl.updateOrderAddressOnShopify(ser.validated_data)
        if status:
            return Response({'detail': "Address updated successfully"}, 200)
        else:
            return Response({'detail': "Address didn't updated successfully"}, 400)