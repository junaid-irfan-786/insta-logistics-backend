from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.Serializers.ServiceTypeSerializer import ServiceTypeSerializer
from Logistics.models import ServiceTypes


class ServiceTypeView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        cities = ServiceTypes.objects.all()
        ser = ServiceTypeSerializer(cities, many=True)
        return Response(ser.data)