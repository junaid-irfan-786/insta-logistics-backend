from rest_framework import serializers, exceptions
from rest_framework.validators import UniqueValidator

from Logistics.CourierBusinessLogic import CourierBusinessLogic
from Logistics.models import CourierCredentials, Settings
from authentication.models import Shop


class CourierCredentialsSerializer(serializers.ModelSerializer):
    api_key = serializers.CharField()

    class Meta:
        model = CourierCredentials
        fields = ('api_key', 'fragile', 'type', 'service_type')

    def validate(self, attrs):
        api_key = attrs.get('api_key')

        cities = CourierBusinessLogic.fetchCities(api_key)
        if len(cities) == 0:
            raise exceptions.ParseError('Invalid Api Key')

        return attrs


class CourierSettingsSerializer(serializers.ModelSerializer):
    mark_paid_on_deliver = serializers.BooleanField()
    shopify_location_id = serializers.CharField()
    shopify_location_address = serializers.CharField()

    class Meta:
        model = Settings
        fields = ('mark_paid_on_deliver', 'shopify_location_id', 'shopify_location_address', 'logo_url')


class SettingsSerializer(serializers.ModelSerializer):
    name = serializers.CharField(validators=[UniqueValidator(queryset=Shop.objects.all())])
    phone_number = serializers.CharField(validators=[UniqueValidator(queryset=Shop.objects.all())])
    email = serializers.EmailField(validators=[UniqueValidator(queryset=Shop.objects.all())])
    address = serializers.CharField()
    city = serializers.CharField()
    courier_credentials = CourierCredentialsSerializer(source="shop_courier")
    courier_settings = CourierSettingsSerializer(source="shop_setting")

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.email = validated_data.get('email', instance.email)
        instance.address = validated_data.get('address', instance.address)
        instance.city = validated_data.get('city', instance.city)

        courier_credentials = validated_data.get('shop_courier')
        credentials, created = CourierCredentials.objects.get_or_create(shop=instance)
        credentials.api_key = courier_credentials.get('api_key', credentials.api_key)
        credentials.type = courier_credentials.get('type', credentials.type)
        credentials.service_type = courier_credentials.get('service_type', credentials.service_type)
        credentials.fragile = courier_credentials.get('fragile', credentials.fragile)
        credentials.save(update_fields=['api_key', 'type', 'service_type', 'fragile'])

        courier_settings = validated_data.get('shop_setting')
        settings, created = Settings.objects.get_or_create(shop=instance)
        settings.mark_paid_on_deliver = courier_settings.get('mark_paid_on_deliver', settings.mark_paid_on_deliver)
        settings.shopify_location_id = courier_settings.get('shopify_location_id', settings.shopify_location_id)
        settings.shopify_location_address = courier_settings.get('shopify_location_address', settings.shopify_location_address)
        settings.logo_url = courier_settings.get('logo_url', settings.logo_url)
        settings.save(update_fields=['mark_paid_on_deliver', 'shopify_location_id', 'shopify_location_address', 'logo_url'])

        instance.save()
        return instance

    class Meta:
        model = Shop
        fields = ('name', 'phone_number', 'email', 'address', 'city', 'courier_credentials', 'courier_settings')
