from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.Serializers.SettingsSerializer import SettingsSerializer


class SettingView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        ser = SettingsSerializer(request.user)
        return Response(ser.data)

    def put(self, request):
        ser = SettingsSerializer(data=request.data, instance=request.user)
        ser.is_valid(raise_exception=True)
        ser.save()
        return Response(ser.data)