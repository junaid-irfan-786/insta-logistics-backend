from rest_framework import serializers


class EditOrderSerializer(serializers.Serializer):
    order_id = serializers.CharField()
    address1 = serializers.CharField()
    address2 = serializers.CharField(allow_null=True, allow_blank=True)
    phone = serializers.CharField(allow_null=True, allow_blank=True)
    city = serializers.CharField()