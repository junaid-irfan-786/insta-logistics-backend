from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.Serializers.UnfulfilledOrderSerializer import UnfulfilledOrderSerializer
from Logistics.ShopifyBusinessLogic import ShopifyBusinessLogic


class UnfulfilledOrders(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        ser = UnfulfilledOrderSerializer(data=request.GET)
        ser.is_valid(raise_exception=True)
        bl = ShopifyBusinessLogic(request.user)
        orders = bl.getSingleObjectFromShopify(ser.validated_data['limit'],
                                               ser.validated_data['cursor'], ser.validated_data['filter'])
        return Response(orders)