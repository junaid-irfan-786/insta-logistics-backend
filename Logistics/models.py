from django.db import models
from authentication.models import Shop


class FulfilledOrders(models.Model):
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, related_name="shop_orders")
    order_id = models.CharField(blank=True, null=True, max_length=200)
    fulfillment_id = models.CharField(blank=True, null=True, max_length=200)
    cn_number = models.CharField(blank=True, null=True, max_length=200)
    cn_generated = models.BooleanField(default=False)
    is_edited = models.BooleanField(default=False)
    fulfilled_on_shopify = models.BooleanField(default=False)
    order_name = models.CharField(blank=True, null=True, max_length=200)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    fulfillment_status = models.CharField(max_length=200, blank=True, null=True)
    financial_status = models.CharField(blank=True, null=True, max_length=200)
    note = models.CharField(blank=True, null=True, max_length=200)
    shipping_charges = models.CharField(max_length=200, default="0.00")
    service_type = models.CharField(max_length=200, null=True, blank=True)
    tags = models.CharField(blank=True, null=True, max_length=200)
    total_price = models.CharField(blank=True, null=True, max_length=200)
    subtotal_price = models.CharField(blank=True, null=True, max_length=200)
    customer_email = models.CharField(blank=True, null=True, max_length=200)
    customer_name = models.CharField(blank=True, null=True, max_length=200)
    customer_phone = models.CharField(blank=True, null=True, max_length=200)
    customer_city = models.CharField(blank=True, null=True, max_length=200)
    customer_address1 = models.CharField(blank=True, null=True, max_length=200)
    customer_address2 = models.CharField(blank=True, null=True, max_length=200)
    tracking_status = models.CharField(blank=True, null=True, max_length=200)


class OrderLineItems(models.Model):
    order = models.ForeignKey(FulfilledOrders, on_delete=models.CASCADE, related_name="fulfilled_lines")
    line_id = models.CharField(max_length=20, blank=True, null=True)
    variant_id = models.CharField(max_length=20, blank=True, null=True)
    product_id = models.CharField(max_length=20, blank=True, null=True)
    product_name = models.CharField(max_length=300, blank=True, null=True)
    product_price = models.CharField(max_length=100, blank=True, null=True)
    product_weight = models.IntegerField(blank=True, null=True)
    product_quantity = models.IntegerField(blank=True, null=True)
    sku_code = models.CharField(max_length=200, blank=True, null=True)
    barcode = models.CharField(max_length=200, blank=True, null=True)
    option1 = models.CharField(max_length=200, blank=True, null=True)
    option2 = models.CharField(max_length=200, blank=True, null=True)
    option3 = models.CharField(max_length=200, blank=True, null=True)


class CourierCredentials(models.Model):
    shop = models.OneToOneField(Shop, on_delete=models.CASCADE, related_name="shop_courier")
    api_key = models.CharField(max_length=100, blank=True, null=True)
    fragile = models.BooleanField(blank=True, null=True, default=False)
    type = models.CharField(max_length=255, blank=False, null=False, default='package')
    service_type = models.CharField(max_length=255, blank=False, null=False, default='24hrs')


class Cities(models.Model):
    name = models.CharField(blank=True, null=True, max_length=200)
    value = models.CharField(blank=True, null=True, max_length=200)


class ServiceTypes(models.Model):
    name = models.CharField(blank=True, null=True, max_length=200)
    value = models.CharField(blank=True, null=True, max_length=200)


class Settings(models.Model):
    shop = models.OneToOneField(Shop, on_delete=models.CASCADE, related_name="shop_setting")
    shopify_location_id = models.CharField(max_length=100, blank=True, null=True)
    shopify_location_address = models.CharField(max_length=500, blank=True, null=True)
    mark_paid_on_deliver = models.BooleanField(default=False)
    stepperCompletion = models.BooleanField(default=False)
    invoice_count = models.PositiveIntegerField(default=1)
    logo_url = models.TextField(blank=True, null=True)


class Logs(models.Model):
    order = models.ForeignKey(FulfilledOrders, on_delete=models.CASCADE, related_name="order_logs")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(blank=True, null=True, max_length=200)
    type = models.CharField(blank=True, null=True, max_length=200)
    detail = models.CharField(blank=True, null=True, max_length=200)
