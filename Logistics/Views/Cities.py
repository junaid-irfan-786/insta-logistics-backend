from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.Serializers.CitiesSerializer import CitiesSerializer
from Logistics.models import Cities


class AllCitiesView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        cities = Cities.objects.all()
        ser = CitiesSerializer(cities, many=True)
        return Response(ser.data)