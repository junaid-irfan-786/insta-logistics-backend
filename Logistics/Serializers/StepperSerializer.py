import requests
from rest_framework import serializers, exceptions
from rest_framework.validators import UniqueValidator

from Logistics.CourierBusinessLogic import CourierBusinessLogic
from Logistics.models import CourierCredentials, Settings
from authentication.models import Shop


class StepOneSerializer(serializers.ModelSerializer):
    name = serializers.CharField(validators=[UniqueValidator(queryset=Shop.objects.all())])
    phone_number = serializers.CharField(validators=[UniqueValidator(queryset=Shop.objects.all())])
    email = serializers.EmailField(validators=[UniqueValidator(queryset=Shop.objects.all())])
    address = serializers.CharField()
    city = serializers.CharField()

    class Meta:
        model = Shop
        fields = ('name', 'phone_number', 'email', 'address', 'city')


class StepTwoSerializer(serializers.ModelSerializer):
    api_key = serializers.CharField()
    fragile = serializers.BooleanField()
    type = serializers.CharField()
    service_type = serializers.CharField()

    class Meta:
        model = CourierCredentials
        fields = ('api_key', 'fragile', 'type', 'service_type')

    def validate(self, attrs):
        api_key = attrs.get('api_key')

        cities = CourierBusinessLogic.fetchCities(api_key)
        if len(cities) == 0:
            raise exceptions.ParseError('Invalid Api Key')

        return attrs


class StepThreeSerializer(serializers.ModelSerializer):
    mark_paid_on_deliver = serializers.BooleanField()
    shopify_location_id = serializers.CharField()
    shopify_location_address = serializers.CharField()

    class Meta:
        model = Settings
        fields = ('mark_paid_on_deliver', 'shopify_location_id', 'shopify_location_address')
