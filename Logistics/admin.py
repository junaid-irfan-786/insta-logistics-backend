from django.contrib import admin
from Logistics.models import Cities, ServiceTypes

admin.site.register(Cities)
admin.site.register(ServiceTypes)