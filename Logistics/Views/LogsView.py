from django.db.models import Q
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from Logistics.Serializers.LogsSerializer import LogsSerializer
from Logistics.models import Logs
from Logistics.pagination import StandardResultSetPagination


class LogsView(ListAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = LogsSerializer
    queryset = Logs.objects.all().select_related('order')
    pagination_class = StandardResultSetPagination

    def get_queryset(self):
        search = self.request.GET.get('key')

        child_search_dic = Q()
        if search is not None:
            my_dic_for_child = {}
            search = search.replace(' ', '')
            search = search.split(',')
            order_list = []
            if len(search) > 1:
                for el in search:
                    order_list.append(el)
            else:
                for el in search:
                    order_list.append(el)

            my_dic_for_child['order__order_name__icontains'] = order_list
            for item, value in my_dic_for_child.items():
                for a in value:
                    child_search_dic |= Q(**{item: a})

        dic = {}
        dic['order__shop'] = self.request.user

        return self.queryset.filter(**dic).filter(child_search_dic).order_by('-id')