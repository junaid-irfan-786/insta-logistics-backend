import requests
from Logistics.models import FulfilledOrders, CourierCredentials
import time
import json

class OrderTrackingLogic(object):

    # def __init__(self, shop):
    #     self.shop = shop
    #     self.courier_credentials = self.shop.shop_courier

    def trackOrderScheduler(self):
        orders = FulfilledOrders.objects.filter(fulfillment_status="fulfilled")
        for order in orders:
            try:
                credentials: CourierCredentials = CourierCredentials.objects.get(shop=order.shop)
            except CourierCredentials.DoesNotExist:
                message = "Insta Logistics Credentials not found for shop {}".format(order.shop.name)
                # OrderLogs.objects.create(order=order,
                #                          description=message,
                #                          dateTime=datetime.datetime.now(), shop=order.shop)
                break

            if credentials is not None:
                try:
                    insta_logistics_request = requests.get(
                        f"https://app.couriermanager.eu/cscourier/API/get_status?api_key={credentials.api_key}&awbno={order.cn_number}")
                except:
                    self.message = "Exception occurred when hitting to Insta Logistics server"
                    # OrderLogs.objects.create(order=order,
                    #                          description=message,
                    #                          dateTime=datetime.datetime.now(), shop=order.shop)

                    break

                result = insta_logistics_request.json()

                if insta_logistics_request.status_code == 200:
                    if len(result['data']['no']) > 0:
                        if len(result['data']['status']) > 0:
                            status = result['data']['status']
                            order.tracking_status = status
                            order.save(update_fields=['tracking_status'])
                            if status.lower() == "delivered":
                                settings = order.shop.shop_setting
                                if settings.mark_paid_on_deliver:
                                    self.mark_as_paid(order)
                        else:
                            order.tracking_status = "Not available"
                            order.save(update_fields=['tracking_status'])

                    else:
                        order.tracking_status = "Not available"
                        order.save(update_fields=['tracking_status'])
                else:
                    self.message = "Insta Logistics server is not responding at the moment and return other than 200 response"
                    # OrderLogs.objects.create(order=order,
                    #                          description=message,
                    #                          dateTime=datetime.datetime.now(), shop=order.shop)
                    break

    def track_rder(self, order):
        try:
            credentials: CourierCredentials = CourierCredentials.objects.get(shop=order.shop)
        except CourierCredentials.DoesNotExist:
            return 404, {"detail": "Credentials not found for shop {}".format(order.shop.name)}

        if credentials is not None:
            try:
                insta_logistics_request = requests.get(
                    f"https://app.couriermanager.eu/cscourier/API/get_status?api_key={credentials.api_key}&awbno={order.cn_number}")
            except:
                return 404, {"detail": "Exception occurred when hitting to Logistics server"}

            result = insta_logistics_request.json()

            if insta_logistics_request.status_code == 200:
                if len(result['data']['no']) > 0:
                    if len(result['data']['status']) > 0:
                        status = result['data']['status']
                        return 200, {"detail": status}
                    else:
                        return 200, {"detail": "Not available"}
                else:
                    return 200, {"detail": "Not available"}
            else:
                return 404, {"detail": "server is not responding at the moment"}

    def mark_as_paid(self, order):
        i = 0
        while i < 3:
            url = "https://" + order.shop.shopify_domain + "/admin/api/2020-04/orders/" + str(order.order_id) + "/transactions.json"
            headers = {
                "X-Shopify-Access-Token": order.shop.shopify_token,
                "Content-Type": "application/json"
            }
            trans = requests.get(url, headers=headers)
            if trans.status_code == 200:
                data = trans.json()
                if len(data["transactions"]) > 0:
                    data = data["transactions"][0]
                    response = requests.post(url, json={
                        "transaction": {
                            "currency": "PKR",
                            "amount": str(order.total_price),
                            "kind": "capture",
                            "parent_id": data["id"]
                        }
                    }, headers=headers)
                    if response.status_code == 201:
                        zx = response
                        break
                    else:
                        i = i + 1
                        time.sleep(2)
            else:
                i = i + 1
                time.sleep(2)
            # link = "https://" + order.shop.shopify_domain + "/admin/api/2020-07/orders/" + order.order_id + ".json"
            # headers = {
            #     "X-Shopify-Access-Token": order.shop.shopify_token,
            #     "Content-Type": "application/json"
            # }
            # data = {
            #     "order": {
            #         "id": order.order_id,
            #         "financial_status": "Paid"
            #     }
            # }
            # resp = requests.put(link, data=json.dumps(data), headers=headers)
            # if resp.status_code == 200:
            #     break