from datetime import timedelta
from rest_framework import serializers


class UnfulfilledOrderSerializer(serializers.Serializer):
    payment_status = serializers.CharField(required=False)
    from_date = serializers.DateField(required=False, format="%Y-%m-%d")
    to_date = serializers.DateField(required=False, format="%Y-%m-%d")
    city = serializers.CharField(required=False)
    search = serializers.CharField(required=False)
    limit = serializers.CharField(default=10)
    cursor = serializers.CharField(default="")

    def validate(self, attrs):
        start = attrs.get('from_date')
        end = attrs.get('to_date')
        payment_status = attrs.get('payment_status')
        city = attrs.get('city')
        search = attrs.get('search')

        filter = "fulfillment_status=unshipped"
        if payment_status is not None:
            filter += "&financial_status=%s" % payment_status

        key = ""
        if city is not None:
            key = city
        if search is not None:
            if key != "":
                key = key + ',' + search
            else:
                key = search

        if key != "":
            filter += "&query=%s" % key

        if start is not None and end is not None:
            end = end + timedelta(days=1)
            filter += "&created_at_min=%s&created_at_max=%s" % (start, end)

        attrs['filter'] = filter
        return attrs