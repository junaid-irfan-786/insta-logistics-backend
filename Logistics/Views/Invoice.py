from datetime import datetime
from django.http import HttpResponse
from django.views import View
from Logistics.Serializers.CourierInvoiceSerializer import CourierInvoiceSerializer
from Logistics.models import FulfilledOrders, Settings, ServiceTypes
from Logistics.utils import render_to_pdf
from authentication.models import Shop


class SingleInvoice(View):
    def get(self, request):
        id = request.GET.get("id")
        domain = request.GET.get("shop")

        ids = [id for id in id.split(',')]
        orders = FulfilledOrders.objects.filter(order_id__in=ids).prefetch_related('fulfilled_lines')

        if len(orders) == 0:
            return HttpResponse("<h1>Invalid Order id</h1>")

        shop = Shop.objects.filter(shopify_domain=domain).first()
        if shop is None:
            return HttpResponse("<h1>Invalid shop</h1>")

        invoice_settings = Settings.objects.filter(shop=shop).values()[0]

        # preserving the order of invoice
        orders = list(orders)
        orders.sort(key=lambda album: ids.index(album.order_id))

        serializeOrder = CourierInvoiceSerializer(orders, many=True)
        serializer_data = serializeOrder.data

        for index, data in enumerate(serializer_data):

            total_weight = 0
            for line_item in data['lineItems']:
                total_weight = total_weight + line_item['product_weight']
                line_item['quantity'] = float(line_item['product_quantity'])
                line_item['price'] = int(float(line_item['product_price']))
                line_item['total_amount'] = line_item['quantity'] * line_item['price']
                if line_item['sku_code'] is not None and line_item['sku_code'] != "":
                    line_item['sku_code'] = ' '.join(
                        line_item['sku_code'][i:i + 12] for i in range(0, len(line_item['sku_code']), 12))
                if line_item['barcode'] is not None and line_item['barcode'] != "":
                    line_item['barcode'] = ' '.join(line_item['barcode'][i:i + 12] for i in
                                                            range(0, len(line_item['barcode']), 12))

            service_type = ServiceTypes.objects.filter(value=data['service_type']).first()

            data['service_type'] = service_type.name if service_type is not None else ""
            data['location_name'] = shop.name
            data['location_address'] = shop.address
            data['location_city'] = shop.city
            data['location_phone'] = shop.phone_number
            data['grams'] = str(round(total_weight / 1000, 1)) + ' Kg'
            data['barcode_amount'] = round(float(data['display_amount']))
            data["printed_at"] = datetime.today().strftime('%d/%m/%Y %I:%M %p')

            # For Shipping Address
            data["provinceAndCountry"] = "Pakistan"

        pdf = render_to_pdf('invoice1.html', {"data": serializeOrder.data, 'invoice_setting': invoice_settings,
                                                    'invoice_count': range(invoice_settings['invoice_count'])})

        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Invoice_" + datetime.today().strftime('%Y%m%d%H%M%S') + ".pdf"
            content = "attachment; filename=" + filename
            response['Content-Disposition'] = content
            return response
        else:
            return HttpResponse("<h1>Error while creating PDF</h1>")