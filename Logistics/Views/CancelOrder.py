from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.BusinessLogic import BusinessLogic
from Logistics.Serializers.CancelOrderSerializer import CancelOrderSerializer
from Logistics.Serializers.FulfillOrderSerializer import FulfillOrderSerializer


class CancelOrderView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        ser = CancelOrderSerializer(data=request.data)
        ser.is_valid(raise_exception=True)
        bl = BusinessLogic(request.user)
        bl.cancelOrders(ser.validated_data['orders'])
        return Response({'detail': 'Order Cancelled'}, 200)