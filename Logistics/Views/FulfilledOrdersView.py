from datetime import datetime, timedelta
from django.db.models import Q
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from Logistics.Serializers.FulfilledSerializer import FulfilledSerializer, FulfilledOrderIdRequiredSerializer
from Logistics.TrackingScheduler import OrderTrackingLogic
from Logistics.models import FulfilledOrders
from Logistics.pagination import StandardResultSetPagination
from rest_framework.views import APIView
from rest_framework.response import Response


class FulfilledOrdersView(ListAPIView):
    queryset = FulfilledOrders.objects.filter(fulfillment_status="fulfilled")
    pagination_class = StandardResultSetPagination
    serializer_class = FulfilledSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        financial_status = self.request.GET.get('financial_status')
        start_creation = self.request.GET.get('from')
        end_creation = self.request.GET.get('to')
        search = self.request.GET.get('key')
        search_type = self.request.GET.get('search_type')

        child_search_dic = Q()
        if search is not None:
            my_dic_for_child = {}
            if search_type == "order_name":
                search = search.replace(' ', '')
                search = search.split(',')
                order_list = []
                if len(search) > 1:
                    for el in search:
                        order_list.append(el)
                else:
                    for el in search:
                        order_list.append(el)

                my_dic_for_child['order_name__icontains'] = order_list
                for item, value in my_dic_for_child.items():
                    for a in value:
                        child_search_dic |= Q(**{item: a})
            elif search_type == "city":
                my_dic_for_child['customer_city__icontains'] = search
            elif search_type == "tag":
                my_dic_for_child['tags__icontains'] = search

            if search_type != "order_name":
                for item in my_dic_for_child:
                    child_search_dic |= Q(**{item: my_dic_for_child[item]})

        dic = {}
        dic['shop'] = self.request.user
        if financial_status is not None:
            temp = financial_status.split(",")
            temp = [x.lower() for x in temp]
            if len(temp) > 1:
                dic["financial_status__in"] = temp
            else:
                dic["financial_status"] = str(financial_status).lower()

        start_creation, end_creation = self.filter_date(start_creation, end_creation)

        if start_creation is not None:
            dic["created_at__range"] = (start_creation, end_creation)

        return self.queryset.filter(**dic).filter(child_search_dic)

    def filter_date(self, start, end):

        if start is not None:
            try:
                start = datetime.strptime(start, '%Y-%m-%d')
            except Exception as e:
                content = {"error": e}
                raise ValidationError(content)

            if end is not None:
                try:
                    end = datetime.strptime(end, '%Y-%m-%d')
                except Exception as e:
                    content = {"error": e}
                    raise ValidationError(content)

                if end < start:
                    content = {"error": "Start Date should be less than End Date"}
                    raise ValidationError(content)
            else:
                end = start

            end = end + timedelta(days=1)

        return start, end


class OrderTrackingView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        ser = FulfilledOrderIdRequiredSerializer(data=request.GET)
        ser.is_valid(raise_exception=True)
        order_id = ser.validated_data['id']
        order = FulfilledOrders.objects.filter(id=order_id).first()
        if order is not None:
            bl = OrderTrackingLogic()
            status, response = bl.track_rder(order)
            return Response(response, status)
        else:
            return Response({'detail': 'order not found'}, 404)