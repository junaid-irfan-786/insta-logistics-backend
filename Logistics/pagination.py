from rest_framework.pagination import CursorPagination


class StandardResultSetPagination(CursorPagination):
    page_size = 1
    ordering = "-id"
    page_size_query_param = 'limit'
