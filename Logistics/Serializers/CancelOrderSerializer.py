from rest_framework import serializers
from Logistics.models import FulfilledOrders


class CancelOrderSerializer(serializers.Serializer):
    order_id = serializers.ListField()

    def validate(self, attrs):
        ids = attrs.get('order_id')

        orders = FulfilledOrders.objects.filter(order_id__in=ids)
        attrs['orders'] = orders
        return attrs