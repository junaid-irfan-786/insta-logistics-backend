import json
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import shopify
import requests
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.models import Settings, CourierCredentials
from authentication.models import Shop
from shopify_app import settings


def index(request):
    return render(request, 'install.html')


def get_token(shop):
    token, created = Token.objects.get_or_create(user=shop)

    if not created:
        utc_now = datetime.now()
        if token.created < utc_now - timedelta(hours=6):
            token.delete()
            token, created = Token.objects.get_or_create(user=shop)

    api_token = "Token " + token.key

    return api_token


# APP URL
def install(request):
    if request.GET.get('shop'):
        shop = request.GET.get('shop')
    else:
        return Response({"detail": "shop not found"}, status=404)

    SHOP = Shop.objects.filter(shopify_domain=shop).first()
    if SHOP is not None:
        if SHOP.status:
            session = shopify.Session(SHOP.shopify_domain, "2019-10", SHOP.shopify_token)
            shopify.ShopifyResource.activate_session(session)
            shopify.Shop.current()
            token = get_token(SHOP)

            react_url = settings.CLIENT + "/?SHOPIFY_APP_API_KEY=" + \
                        settings.SHOPIFY_APP_API_KEY + "&shop=" + \
                        shop + "&API_TOKEN=" + token + "&stepper_completion=" \
                        + str(SHOP.shop_setting.stepperCompletion)

            return redirect(react_url)
        else:
            auth_url = "https://" + shop + "/admin/oauth/authorize?client_id=" + \
                       settings.SHOPIFY_APP_API_KEY + "&scope=" + \
                       settings.SHOPIFY_APP_API_SCOPE + "&redirect_uri=" + \
                       settings.REDIRECT_URI

            return redirect(auth_url)

    else:
        auth_url = "https://" + shop + "/admin/oauth/authorize?client_id=" + \
                   settings.SHOPIFY_APP_API_KEY + "&scope=" + \
                   settings.SHOPIFY_APP_API_SCOPE + "&redirect_uri=" + \
                   settings.REDIRECT_URI

        return redirect(auth_url)


# REDIRECT_URI (Whitelisted URL)
def connect(request):
    # IF shop is present in the callback then request for the access token and save the shop and token in Database
    if request.GET.get('shop'):
        shop = request.GET.get('shop')

        params = {
            "client_id": settings.SHOPIFY_APP_API_KEY,
            "client_secret": settings.SHOPIFY_APP_API_SECRET,
            "code": request.GET.get('code')
        }
        resp = requests.post("https://" + shop + "/admin/oauth/access_token", data=params)
        if 200 == resp.status_code:
            resp_json = resp.json()
            try:
                SHOP = Shop.objects.get(shopify_domain=shop)
                SHOP.shopify_token = resp_json['access_token']
                SHOP.status = True
                SHOP.save()

            except Shop.DoesNotExist:
                SHOP = Shop()
                SHOP.shopify_domain = shop
                SHOP.username = shop
                SHOP.status = True
                SHOP.shopify_token = resp_json['access_token']
                SHOP.save()
                Settings.objects.create(shop=SHOP)
                CourierCredentials.objects.create(shop=SHOP)

            createWebHook(SHOP)
            token = get_token(SHOP)

            react_url = settings.CLIENT + "/?SHOPIFY_APP_API_KEY=" + \
                        settings.SHOPIFY_APP_API_KEY + "&shop=" + \
                        shop + "&API_TOKEN=" + token + "&stepper_completion=" + str(False)

            return redirect(react_url)

        else:
            return render(request, 'not_found.html')


class uninstallView(APIView):
    def post(self, request):
        myshopify_domain = request.data['myshopify_domain']
        shop = Shop.objects.filter(shopify_domain=myshopify_domain).first()
        if shop is not None:
            shop.status = False
            shop.save(update_fields=['status'])
        return Response("App uninstalled")


# Create Webhooks for orders
def createWebHook(SHOP):
    headers = {
        "X-Shopify-Access-Token": SHOP.shopify_token,
        "Content-Type": "application/json"
    }
    link = "https://" + SHOP.shopify_domain + "/admin/api/" + settings.SHOPIFY_API_VERSION + "/webhooks.json"

    # Parameters to create product create webhook
    params = {
        "webhook": {
            "topic": "app/uninstalled",
            "address": settings.HOST + "/auth/uninstall",
            "format": "json"
        }
    }
    requests.post(link, json=params, headers=headers)

    return HttpResponse("Webhook created")


@csrf_exempt
def ChargePlan(request):
    domain = request.GET.get("shop")
    plan_name = request.GET.get("plan_name")
    shop = Shop.objects.get(shopify_domain=domain)
    shop_obj = Shop.objects.filter(shopify_domain=domain).first()

    if plan_name == "Free":
        token = get_token(shop)
        shop.plan_name = "Free"
        shop.prev_charge_id = None
        shop.prev_confirmation_url = None
        shop.prev_fulfillment_count = None
        shop.prev_fulfillment_allowed = None
        shop.prev_fulfillment_reached_package_end = 0
        shop.prev_current_month_date = None
        shop.save()
        react_url = settings.CLIENT + "/?SHOPIFY_APP_API_KEY=" + \
                    settings.SHOPIFY_APP_API_KEY + "&shop=" + \
                    domain + "&API_TOKEN=" + \
                    token + "&plan_name=" + \
                    shop.plan_name
        return redirect(react_url)
    charge = PlanCharge(shop, plan_name)
    context = {'url': charge["confirmation_url"]}
    return render(request, 'confirmationRedirect.html', context)


# Create a charge for Shop if shop has been uninstalled
def PlanCharge(SHOP, plan):
    domain = SHOP.shopify_domain
    token = SHOP.token
    session = shopify.Session(domain, "2019-10", token)
    shopify.ShopifyResource.activate_session(session)
    charge = shopify.RecurringApplicationCharge()
    remaining_orders = SHOP.fulfillment_allowed - SHOP.fulfillment_count
    if plan == "Starter":
        charge.name = "Starter"
        charge.price = 9
        # fulfillment_allow = 100 + remaining_orders
        fulfillment_allow = 100
        package_limit = 100

    elif plan == "Lite":
        charge.name = "Lite"
        charge.price = 19
        # fulfillment_allow = 250 + remaining_orders
        fulfillment_allow = 250
        package_limit = 250

    elif plan == "Startup":
        charge.name = "Startup"
        charge.price = 39
        # fulfillment_allow = 500 + remaining_orders
        fulfillment_allow = 500
        package_limit = 500

    elif plan == "Bronze":
        charge.name = "Bronze"
        charge.price = 99
        # fulfillment_allow = 1500 + remaining_orders
        fulfillment_allow = 1500
        package_limit = 1500

    elif plan == "Silver":
        charge.name = "Silver"
        charge.price = 149
        # fulfillment_allow = 2500 + remaining_orders
        fulfillment_allow = 2500
        package_limit = 2500

    elif plan == "Gold":
        charge.name = "Gold"
        charge.price = 199
        # fulfillment_allow = 5000 + remaining_orders
        fulfillment_allow = 5000
        package_limit = 5000

    elif plan == "Platinum":
        charge.name = "Platinum"
        charge.price = 349
        # fulfillment_allow = 10000 + remaining_orders
        fulfillment_allow = 10000
        package_limit = 10000

    elif plan == "Mega":
        charge.name = "Mega"
        charge.price = 489
        # fulfillment_allow = 15000 + remaining_orders
        fulfillment_allow = 15000
        package_limit = 15000

    elif plan == "Ultimate":
        charge.name = "Ultimate"
        charge.price = 929
        # fulfillment_allow = 15000 + remaining_orders
        fulfillment_allow = 15000
        package_limit = 15000

    elif plan == "Custom":
        charge.name = "Custom"
        if float(SHOP.custom_charge_amount) == 0:
            charge.price = 0.01
        else:
            charge.price = float(SHOP.custom_charge_amount)
        # fulfillment_allow = int(SHOP.custom_orders) + remaining_orders
        fulfillment_allow = int(SHOP.custom_orders)
        package_limit = int(SHOP.custom_orders)

    charge.return_url = settings.CALLBACK_URL + '?shop=' + SHOP.shopify_domain

    # charge.test = True

    charge.save()
    charge_json = charge.attributes
    Shop.objects.filter(shopify_domain=domain).update(charge_id=charge_json["id"],
                                                        plan_name=plan,
                                                        confirmation_url=charge_json["confirmation_url"],
                                                        fulfillment_count=0,
                                                        fulfillment_allowed=fulfillment_allow,
                                                        package_limit=package_limit,
                                                        fulfillment_reached_package_end=False,
                                                        current_month_date=datetime.now(),
                                                        prev_charge_id=SHOP.charge_id,
                                                        prev_plan_name=SHOP.plan_name,
                                                        prev_confirmation_url=SHOP.confirmation_url,
                                                        prev_fulfillment_count=SHOP.fulfillment_count,
                                                        prev_fulfillment_allowed=SHOP.fulfillment_allowed,
                                                        prev_fulfillment_reached_package_end=SHOP.fulfillment_reached_package_end,
                                                        prev_current_month_date=SHOP.current_month_date)
    return charge_json


def MorePlans(request):
    return render(request, 'price_plan.html')


# A Custom charge request from merchants which changes the status of custom_charge_request field in the Shop table.
class CustomChargeRequest(APIView):
    def get(self, request):
        if "shop" in request.GET:
            try:
                shop = Shop.objects.get(shopify_domain=request.GET.get('shop'))
            except Shop.DoesNotExist:
                return Response({"detail": "Shop does not exists"}, status=404)

            SHOP = Shop.objects.get(shopify_domain=shop)
            if SHOP.shopify_domain is not None:
                SHOP.custom_charge_requested = True
                SHOP.save()
                context = {
                    "detail": "Your request will be processed soon!"
                }
                return Response(data=context, status=status.HTTP_200_OK)
            else:
                return Response({"detail": "Shop not found"}, status=404)


# Shops Data
class ShopInformation(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        if token_validation(request):
            fetch_charge_status()
            shops_data = []
            shops = Shop.objects.all()
            total_plan_price = 0.0
            total_earned_price = 0.0
            total_install_count = 0
            total_uninstalled_count = 0
            total_test_install_count = 0
            test_charge_count = 0
            active_charge_count = 0
            inactive_charge_count = 0

            for shop in shops:
                headers = {
                    "X-Shopify-Access-Token": shop.token,
                    "Content-Type": "application/json"
                }
                shop_get_link = "https://" + shop.shopify_domain + "/admin/api/" + settings.SHOPIFY_API_VERSION + "/shop.json"
                shop_resp = requests.get(shop_get_link, headers=headers)
                try:
                    shop_data = json.loads(shop_resp.text)
                except Exception as e:
                    print(e)
                    return Response({"message": "Some Error Occurred"}, status=400)
                id = shop.id
                shop_name = shop.shop_name
                shop_owner = shop.shop_owner
                shop_email = shop.shop_email
                shop_number = shop.shop_number
                charge_id = shop.charge_id
                plan_name = shop.plan_name
                fulfillment_allowed = shop.fulfillment_allowed
                fulfillment_count = shop.fulfillment_count
                remaining_orders = fulfillment_allowed - fulfillment_count
                shopify_domain = shop.shopify_domain
                date_joined = shop.date_joined
                charge_status = shop.charge_status

                if plan_name == "Starter":
                    plan_price = 9
                elif plan_name == "Lite":
                    plan_price = 19
                elif plan_name == "Startup":
                    plan_price = 39
                elif plan_name == "Bronze":
                    plan_price = 99
                elif plan_name == "Silver":
                    plan_price = 149
                elif plan_name == "Gold":
                    plan_price = 199
                elif plan_name == "Platinum":
                    plan_price = 349
                elif plan_name == "Mega":
                    plan_price = 489
                elif plan_name == "Ultimate":
                    plan_price = 929
                elif plan_name == "Custom":
                    plan_price = float(shop.custom_charge_amount)
                else:
                    plan_price = 0.0

                if 'errors' in shop_data:
                    plan_price = 0.0
                    installation_status = "Un-Installed"
                    earned_price = 0.0
                    total_uninstalled_count = total_uninstalled_count + 1
                    test_charge_count = test_charge_count + 1
                else:
                    shop_name = shop_data['shop']['name']
                    shop_owner = shop_data['shop']['shop_owner']
                    shop_email = shop_data['shop']['email']
                    shop_number = shop_data['shop']['phone']
                    shop_country = shop_data['shop']['country_name']
                    shop_city = shop_data['shop']['city']
                    shop_address = shop_data['shop']['address1']
                    shop_zip = shop_data['shop']['zip']

                    shop.shop_name = shop_name
                    shop.shop_owner = shop_owner
                    shop.shop_email = shop_email
                    shop.shop_number = shop_number
                    shop.shop_country = shop_country
                    shop.shop_city = shop_city
                    shop.shop_address = shop_address
                    shop.shop_zip = shop_zip
                    shop.save()

                    if plan_price != 0:
                        if shopify_domain == 'alchemative-plussandbox.myshopify.com' or shopify_domain == 'obaid-b.myshopify.com' or shopify_domain == 'jomo-staging.myshopify.com' or shopify_domain == 'obaid5319365.myshopify.com' or shopify_domain == 'testcloudstore.myshopify.com' or shopify_domain == 'baroque1.myshopify.com':
                            plan_price = 0.0
                            earned_price = 0.0
                            installation_status = "Test-Install"
                            total_test_install_count = total_test_install_count + 1
                            test_charge_count = test_charge_count + 1
                        else:
                            if shop.charge_status == "active":
                                comission = round(float((plan_price / 100) * 20), 0)
                                earned_price = plan_price - comission
                                total_plan_price = total_plan_price + plan_price
                                total_earned_price = total_earned_price + earned_price
                                installation_status = "Installed"
                                total_install_count = total_install_count + 1
                                active_charge_count = active_charge_count + 1
                            else:
                                earned_price = 0.0
                                total_plan_price = total_plan_price + plan_price
                                total_earned_price = total_earned_price + earned_price
                                installation_status = "Installed"
                                total_install_count = total_install_count + 1
                                inactive_charge_count = inactive_charge_count + 1
                    else:
                        if shopify_domain == 'alchemative-plussandbox.myshopify.com' or shopify_domain == 'obaid-b.myshopify.com' or shopify_domain == 'jomo-staging.myshopify.com' or shopify_domain == 'obaid5319365.myshopify.com' or shopify_domain == 'testcloudstore.myshopify.com' or shopify_domain == 'baroque1.myshopify.com':
                            plan_price = 0.0
                            earned_price = 0.0
                            installation_status = "Test-Install"
                            total_test_install_count = total_test_install_count + 1
                            test_charge_count = test_charge_count + 1
                        else:
                            if shop.charge_status == "active":
                                comission = round(float((plan_price / 100) * 20), 0)
                                earned_price = plan_price - comission
                                total_plan_price = total_plan_price + plan_price
                                total_earned_price = total_earned_price + earned_price
                                installation_status = "Installed"
                                total_install_count = total_install_count + 1
                            else:
                                earned_price = 0.0
                                total_plan_price = total_plan_price + plan_price
                                total_earned_price = total_earned_price + earned_price
                                installation_status = "Installed"
                                total_install_count = total_install_count + 1
                                inactive_charge_count = inactive_charge_count + 1

                temp_obj = {
                    'id': id,
                    'shop_name': shop_name,
                    'shop_owner': shop_owner,
                    'shop_email': shop_email,
                    'shop_number': shop_number,
                    'charge_id': charge_id,
                    'plan_name': plan_name,
                    'plan_price': plan_price,
                    'earned_price': earned_price,
                    'shopify_domain': shopify_domain,
                    'charge_status': charge_status,
                    'store_name': shopify_domain.split('.')[0],
                    'installation_date': date_joined.strftime("%d %B %Y %I:%M %p"),
                    'installation_status': installation_status,
                    'fulfillment_allowed': fulfillment_allowed,
                    'fulfillment_count': fulfillment_count,
                    'remaining_message': remaining_orders
                }
                shops_data.append(temp_obj)
                print("Shopify Store: " + shop.shopify_domain)
            context = {
                'shops': shops_data,
                'total_plan_price': total_plan_price,
                'total_earned_price': round(total_earned_price, 0),
                'total_installed': total_install_count,
                'total_uninstalled': total_uninstalled_count,
                'total_test_install': total_test_install_count,
                'test_charge_count': test_charge_count,
                'active_charge_count': active_charge_count,
                'inactive_charge_count': inactive_charge_count
            }
            return Response(data=context, status=200)
        else:
            return Response({"message": "Invalid Token"}, status=422)


def token_validation(request):
    if "HTTP_AUTHORIZATION" in request.META:
        api_key = request.META.get('HTTP_AUTHORIZATION')
        if api_key == 'Bearer 53f262ca2a68fca55afd460667fc011c6ff41b':
            return True
        else:
            return False
    else:
        return False


def fetch_charge_status():
    shops = Shop.objects.filter(charge_status = 'not active')
    for shop in shops:
        headers = {
            "X-Shopify-Access-Token": shop.token,
            "Content-Type": "application/json"
        }
        charge_get_link = "https://" + shop.shopify_domain + "/admin/api/" + settings.SHOPIFY_API_VERSION + "/recurring_application_charges/" + str(
            shop.charge_id) + ".json"
        charge_resp = requests.get(charge_get_link, headers = headers)
        charge_data = json.loads(charge_resp.text)
        try:
            charge_json = charge_data['recurring_application_charge']

            if charge_json["status"] == "active":
                shop.charge_status = "active"
                shop.charge_response = charge_json["status"]
                shop.save()
                print("Shop: " + shop.shopify_domain + " Charge Status: " + charge_json["status"])
            else:
                print("Shop: " + shop.shopify_domain + " Charge Status: " + charge_json["status"])
        except:
            charge_json = charge_data['errors']
            shop.charge_response = charge_json
            shop.save()
            print("Shop: " + shop.shopify_domain + " Charge Status: " + charge_json)


class CustomCharge(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        if token_validation(request):
            try:
                shop = request.data["shop"]
                custom_charge_amount = request.data["customChargeAmount"]
                custom_orders = request.data["customOrders"]
                expiry_status = False
                if request.data["expiry_status"]:
                    expiry_status = True
                    months = request.data["expiry_months"]
                    expiry_date = datetime.now()+relativedelta(months=+months)
            except Exception as e:
                print(e)
                return Response({"detail": "Parameter missing!"}, status=404)

            SHOP = Shop.objects.get(shopify_domain=shop)
            if SHOP.shopify_domain is not None:
                SHOP.activate_custom_charge = True
                SHOP.custom_charge_amount = custom_charge_amount
                SHOP.custom_orders = custom_orders
                SHOP.custom_charge_expiration_status = expiry_status
                if expiry_status:
                    SHOP.custom_charge_expiration_date = expiry_date
                SHOP.save()
                context = {
                    "detail": "Custom charge created! User can upgrade to new charge!"
                }
                return Response(data=context, status=200)
            else:
                return Response({"detail": "Shop not found"}, status=404)
        else:
            return Response({"message": "Invalid Token"}, status=422)