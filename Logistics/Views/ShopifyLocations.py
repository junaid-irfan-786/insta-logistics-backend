from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.ShopifyBusinessLogic import ShopifyBusinessLogic


class ShopifyLocationsView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        bl = ShopifyBusinessLogic(request.user)
        locations = bl.getLocationsFromShopify()
        return Response(locations)