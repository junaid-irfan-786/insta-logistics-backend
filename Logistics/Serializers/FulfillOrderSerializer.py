from rest_framework import serializers


class LineItemSerializer(serializers.Serializer):
    variant_id = serializers.CharField()
    product_id = serializers.CharField()
    line_id = serializers.CharField()
    product_name = serializers.CharField()
    product_price = serializers.CharField()
    product_weight = serializers.IntegerField()
    product_quantity = serializers.IntegerField()
    sku_code = serializers.CharField()
    option1 = serializers.CharField(required=False)
    option2 = serializers.CharField(required=False)
    option3 = serializers.CharField(required=False)
    barcode = serializers.CharField(required=False)


class FulfillOrderSerializer(serializers.Serializer):
    order_id = serializers.CharField()
    order_name = serializers.CharField()
    fulfillment_status = serializers.CharField(allow_null=True, allow_blank=True)
    financial_status = serializers.CharField()
    note = serializers.CharField(allow_blank=True, allow_null=True)
    tags = serializers.CharField(allow_blank=True, allow_null=True)
    total_price = serializers.CharField()
    subtotal_price = serializers.CharField()
    shipping_charges = serializers.CharField(allow_null=True, allow_blank=True)
    service_type = serializers.CharField()
    created_at = serializers.DateTimeField()
    updated_at = serializers.DateTimeField()
    customer_email = serializers.CharField(allow_blank=True, allow_null=True)
    customer_name = serializers.CharField()
    customer_phone = serializers.CharField(allow_blank=True, allow_null=True)
    customer_city = serializers.CharField()
    customer_address1 = serializers.CharField()
    customer_address2 = serializers.CharField(allow_null=True, allow_blank=True)
    line_items = LineItemSerializer(many=True)

    def validate(self, attrs):
        shipping_charges = attrs.get('shipping_charges')
        if shipping_charges is None or shipping_charges == "":
            attrs['shipping_charges'] = '0'

        return attrs
