from apscheduler.schedulers.background import BackgroundScheduler
from Logistics.TrackingScheduler import OrderTrackingLogic


def every_12_am():
    bl = OrderTrackingLogic()
    scheduler = BackgroundScheduler()
    scheduler.add_job(
        bl.trackOrderScheduler(), 'cron',
        day='*',
        hour='0',
        minute='0'
    )
    scheduler.start()