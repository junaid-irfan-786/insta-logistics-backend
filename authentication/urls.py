from django.contrib import admin
from django.urls import path
from authentication import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('install/', views.install, name='install'),
    path('connect/', views.connect),
    path('uninstall', views.uninstallView.as_view()),
    path('create-subscription', views.ChargePlan),
    path('more_pricing_plans', views.MorePlans, name='MorePlans'),
    path('custom_charge_request', views.CustomChargeRequest.as_view(), name='CustomChargeRequest'),
    path('shopify_stores', views.ShopInformation.as_view(), name='ShopInformation'),
    path('create_custom_charge', views.CustomCharge.as_view(), name='CustomCharge'),
]
