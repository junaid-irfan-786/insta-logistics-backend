from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.Serializers.StepperSerializer import StepOneSerializer, StepTwoSerializer, StepThreeSerializer


class StepOneView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        ser = StepOneSerializer(request.user)
        return Response(ser.data)

    def put(self, request):
        ser = StepOneSerializer(data=request.data, instance=request.user)
        ser.is_valid(raise_exception=True)
        ser.save()
        return Response(ser.data)


class StepTwoView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        ser = StepTwoSerializer(request.user.shop_courier)
        return Response(ser.data)

    def put(self, request):
        ser = StepTwoSerializer(data=request.data, instance=request.user.shop_courier)
        ser.is_valid(raise_exception=True)
        ser.save()
        return Response(ser.data)


class StepThreeView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        ser = StepThreeSerializer(request.user.shop_setting)
        return Response(ser.data)

    def put(self, request):
        setting = request.user.shop_setting
        ser = StepThreeSerializer(data=request.data, instance=setting)
        ser.is_valid(raise_exception=True)
        ser.save()
        setting.stepperCompletion = True
        setting.save(update_fields=['stepperCompletion'])
        return Response({'stepperCompletion': True})