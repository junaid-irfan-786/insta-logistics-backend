from rest_framework import serializers
from Logistics.models import Cities


class CitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cities
        fields = ('name', 'value')