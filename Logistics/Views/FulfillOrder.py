from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from Logistics.BusinessLogic import BusinessLogic
from Logistics.Serializers.FulfillOrderSerializer import FulfillOrderSerializer


class FulfillOrderView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        ser = FulfillOrderSerializer(data=request.data, many=True)
        ser.is_valid(raise_exception=True)
        bl = BusinessLogic(request.user)
        bl.fulfillOrders(ser.data)
        return Response({'detail': "Order Fulfilled"}, 200)